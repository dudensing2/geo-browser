.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
Datasheet Editor FAQ
====================


.. topic:: Why can Geolocation Completion not find the places I added to the table?

    The names of places have to be added to the “Address” column and capitalized to be found via Geolocation Completion. If these two steps are followed and the location is still missing, its name might not be registered in Getty.


.. topic:: Why is my Datasheet Editor data displayed incompletely in Geo-Browser?

    Some HTML-Symbols like “&” confuse the Geo-Browser. Please check your entries for special characters and replace “&” with “&amp” for example. For more information you can visit `Bug #14359 <https://projects.gwdg.de/issues/14359>`__ in our Geo-Browser-Bugtracker.


.. topic:: Is it possible to only add coordinates to certain places by using the set all places named-button?

    No unfortunately not. This feature is planned for an upcoming version (please refer to `Feature #15318 <https://projects.gwdg.de/issues/15318>`__). In the meantime you can temporarily change the names of places, you plan the same coordinates for, and then set their coordinates. If you entered “Berlin” multiple times and want to change the coordinates for some of them, you can either change them into a different place or change their name slightly, so that they are not recognized as Berlin. After that you can return to the original name. This process has to be repeated every time you edit the names.


.. topic:: The automatically generated geographic data by Getty ID is partially incorrect. Why do some places appear to be up to 40 km removed from their origin, making them unusable for further examination?

    These inaccuracies are the result of matching the entered city to the coordinates of the administrative district by the same name. The displayed coordinates are therefore not wrong, but match a different definition of the given name. If Datasheet Editor finds multiple definitions of the entered name, you can change the selection at “Place Selection”. Alternatively you can also manually move the flag-marker on your map.
