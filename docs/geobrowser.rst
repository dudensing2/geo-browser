.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


=========================
The DARIAH-DE Geo-Browser
=========================

The `DARIAH-DE Geo-Browser <https://geobrowser.de.dariah.eu>`__ allows a comparative visualization of several requests and facilitates the representation of data and their visualization in a correlation of geographic spatial relations at corresponding points of time and sequences. Thus, researchers can analyze space-time relations of data and collections of source material and simultaneously establish correlations between them. This `sample dataset <https://geobrowser.de.dariah.eu/?kml1=http://geobrowser.de.dariah.eu/data/schiller.kml&kml2=http://geobrowser.de.dariah.eu/data/goethe.kml>`__ for the Geo-Browser shows publishing dates and locations of works by Friedrich Schiller and Johann Wolfgang von Goethe plotted against each other.

The `DARIAH-DE Geo-Browser <https://geobrowser.de.dariah.eu>`__ is freely accessible on the internet: https://geobrowser.de.dariah.eu. All functions are provided without any registration.
Geo-Browser is a tool for the visualization and analysis of space-time-relations for humanities scholars. The Geo-Browser shows a map of the world. By scrolling you can zoom in and out. On the left side is a zoom-bar, with which you can zoom in and out, by pulling the cursor to + or -.

.. figure:: images/image_1.png

More information concerning Geo-Browser and Datasheet Editor you can find at the `DARIAH-DE Portal <https://de.dariah.eu/en/geobrowser>`__.


GUI Documentation
-----------------

Load Data
^^^^^^^^^

In the upper part you can choose, which kind of data you want to upload in the geo-browser. With “Static Data” you can choose sample data. They are extracted from WebOPAC Göttingen, `Flickr <https://www.flickr.com/>`__, the `Internet Movie Database <https://www.imdb.com/>`__ and from `Wikidata <https://www.wikidata.org/wiki/Wikidata:Main_Page>`__ and already preprocessed for the Geo-Browser.
Satic Flickr Data is using data from Flickr. Place and time of the user shots are recorded here. (Example for a search: Concerts of a certain band), “Political data” includes examples of political data. Static IMDB data uses data of the “Internet Movie Database“ and extracts the metadata time and place of the movie production. “DBPedia Queries“ is a collection of structured information of Wikipedia (https://wiki.dbpedia.org/About).

.. figure:: images/image_2.png

On “KML file URL“ you can upload KML files via the URL. KML files can be created with the  `GoogleMaps-Engine <https://mapsengine.google.com/map/?hl=de>`__, among others. Here you can create a map which can be downloaded. The downloaded data is a KMZ file, which can be converted to a KML file with e.g. `7-zip <https://www.7-zip.org/>`__, or you can upload the KMZ file directly. For uploading KML files from your local hard drive, you have to choose “local KML file”. You can also do this with a CSV file. Note: When you are using Windows, you can create CSV files with e.g. Microsoft Excel or OpenOffice Calc. When you are using Mac OS X you can create CSV files with Numbers. The loaded data is represented on the map by colored circles. By double clicking a colored circle, the category and the number of hits for this specific area will be displayed.

.. figure:: images/image_3.png

When different categories are uploaded, they will be displayed in different colors. At the bottom right of the map, a legend with colors and the corresponding categories will appear.


Load Overlay
^^^^^^^^^^^^

On “Load Overlay“ in the upper field you have to choose the data type of the file which you want to use as the overlay. An overlay is a graphical map which you can superimpose on the original map. For example you can load locations from a current map and create an Overlay of a historical map, and therefore see the current and historical positions of the locations. You can also use URL’s from KML and KMZ files, using the options “KML File URL“ or “KMZ File URL“.

By clicking on “ArcGIS WMS“ you can specify a WMS-Server-URL and a layer which should be shown.

Besides the latitude and longitude, the “XYZ Layer“ also provides "thickness". You can use a URL with x/y/z placeholders. (It is the same format as used in OpenLayers.) “Roman Empire“ imports a map of the Roman Empire.


Dataset Information
^^^^^^^^^^^^^^^^^^^

Once the data is loaded in the Geo- Browser, it can be opened in “Dataset” as “Magnetic Link”. By clicking on “Magnetic Link” every currently referenced dataset can be called again, always in a new tab in your browser. Local data you can store to the DARIAH storage using the "[store to DARIAH storage]" button. Your file then gets an ID and can be shared with others using the magnetic link.


Background Map
^^^^^^^^^^^^^^

There are several options for the map in the background. There is a current map “Contemporary Map (2014)“, there are historical maps, the oldest map is from 2000 BC. There is a map of the “Barrington Roman Empire“, a map of the antiquity in between 550 BC and 640 AD. Open Street Map (Map Quest) is set by default, which shows streets, national borders, mountains, vegetation and deserts.


Map Selector Tools
^^^^^^^^^^^^^^^^^^

.. figure:: images/mapselectortools.png

For marking locations on the maps with geometrical forms, you can use the Map Selector Tools . By clicking on a form, this from can be marked on the map. Here you can choose a rectangle, a circle or a flexible form with the possibility to mark dots on the map by clicking, which get connected . For completing the form you have to click twice on the last dot. Once an area is chosen, all the locations within are marked on the list of results.

.. figure:: images/image_4.png

Furthermore there is the posibility  to mark states - currently this is only working with the historical maps.


Filter
^^^^^^

.. figure:: images/filterminus.png

For filtering specific data in an area, previously marked with the Map Selector Tool, you can use this filter . The data will then be invisible in the Geo-Browser.


Publication Place
^^^^^^^^^^^^^^^^^

“Publication Place“ shows how many results exists for search queries in various places.


Timeline and List of Results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When data is loaded in the Geo Browser, a timeline appears under the map. This timeline shows a chronological sequence of the data. Under the timeline is a list of results.

.. figure:: images/timestart.png

With “Time start“ you can choose a starting time for the timeline.

.. figure:: images/timeunit.png

The “Time unit“ can be chosen here: The options range from “continuous“ to 500 years. The chosen unit will be displayed as boxes. With the selection window "Scaling" you can change the scale of the graph from "normal" to "logarithm" and "percentage".

.. figure:: images/image_5.png


Animation
^^^^^^^^^

To start an animation, you have to choose an area in the timeline. You can choose it by clicking in the timeline.

.. figure:: images/animation.png

With the Play-button you can start a demonstration: The marked area passes through the timeline from left to right through the timeline, while the locations which are linked to a certain time span blink on the map. Below the timeline you can see an orange box.


.. figure:: images/schliessen.png

Clicking this button will delete the loaded data.


.. figure:: images/herunterladenorange.png

Clicking this button allows you to download them as a KML file. Below the orange box is a list of results.


.. figure:: images/hochklappen.png

For hiding them you can use this button. You can choose how many results you want to see on one page (10 to 100).


.. figure:: images/weiter.png

You can see the following page by clicking this button.


.. figure:: images/Bild 1.png

Shows you the first page.


.. figure:: images/N.png

Shows you the last page.


Maps
----

Unless differently indicated, the Geo-Browser uses free maps and charts which are made available through the `DARIAH-DE GeoServer <https://ref.de.dariah.eu/geoserver/>`__, e.g. the historical maps were used within the scope of the project europeana4D for the predecessor of the Geo-Browser (`e4D <https://www.sub.uni-goettingen.de/en/projects-research/project-details/projekt/europeanaconnect-europeana4d/>`__). The historical Maps of `ThinkQuest Team C006628 <https://web.archive.org/web/20060630061554/http://library.thinkquest.org/C006628/>`__ are “in most cases only meant to be used on a large scale, so that only the relative position of countries should be trusted. It is estimated that political boundaries have an average error of 40 miles.“ (see `Disclaimer <https://web.archive.org/web/20060903200541/http://library.thinkquest.org/C006628/disclaimer.html>`__)

Other used maps are integrated online by other suppliers / services, for example, maps of OpenStreetMap and MapQuest. License information can be found at the bottom of the map view.

The map of the Roman Empire (`Digital Atlas of the Roman Empire <https://dare.ht.lu.se/>`__) was developed by Johan Åhlfeldt, supported by the project `Pelagios <http://commons.pelagios.org/>`__. Further information about the project can be found in the article `A digital map of the Roman Empire <http://pelagios-project.blogspot.com/2012/09/a-digital-map-of-roman-empire.html>`__. Further license information can be found `here <http://pelagios.org/maps/greco-roman/about.html>`__.


Versions and Releases
---------------------

You can find the current version of the Geo-Browser here: https://geobrowser.de.dariah.eu. As of June 2020 we arrived at release version 2.7.8. New versions will in principle be compatible with older versions.


Release History
^^^^^^^^^^^^^^^

Changes in Release Version 2.7.8 (June 2020):

* Popup labels on mouseOver the Place circles now first take the “name“ value, and if empty the “place“ value. If both are not set, “unknown“ is used.
* Documentation slightly updated.

Changes in Release Version 2.7.0 (April 2019):

* Update of the `GeoServers <https://ref.de.dariah.eu/geoserver/>`__ to Version 2.13.1.
* Documentation of the CSF file specification
* English translation of the FAQ section
* Content update of documentation
* Usage of the `Sphinx framework <http://sphinx-doc.org/>`__ for generation of documentation

Changes in Release Version 2 (July 2016):

* The `GeoServer <https://ref.de.dariah.eu/geoserver/>`__, that is providing the Geo-Browser with maps, has been updated to version 2.8.3.
* The continental borders of the historical maps have been corrected (GeoServer).
* Geo-Browser now does provide current EuroStat maps of 2013 and 2014 (GeoServer).
* The documentation has been translated to English.
* Licensing information is now provided directly in the map view (PLATIN).
* There is a possibillity to visualise local XLS/XLSX files using “Load Data“ (PLATIN).
* Under „Load Overlay“ the Water Layer of Maps-For-Free can be used (PLATIN).
* A global DARIAH-DE Tool-Menü has been integrated.
* The PIWIK of GWDG was integrated, to track website-usage and browser information (taking data privacy into account). You can deactivate the tracking at the `Privacy Policy Page <https://de.dariah.eu/en/datenschutz>`__ page, or just cofigure your browser accordingly.
* The usage of `OpenGeoNames <http://www.geonames.org/>`__ in the Datasheet Editor is again possible.
* The code of Geo-Browser and Datasheet Editor has been merged into a `shared code repository <https://projects.gwdg.de/projects/geo-browser/repository>`__.
* `PLATIN <https://github.com/DARIAH-DE/PLATIN>`__ has been forked on Github for the usage of Geo-Browser in DARIAH-DE.
* The static data has been added to the PLAITN fork as well.

Changes in Release Version 1 (October 2015):

* Adapted to the DARIAH-DE Styleguide.
* Added information to documentation and FAQ.
* Update of the timeline module, so that time ranges can now be visualised, too.


Specification for Use
---------------------

* :download:`KML Specification <examples/M3.3.2_eConnect_KML_Specification_v1.0_UGOE.pdf>` (PDF)
* :doc:`CSV Specification <csvspec>`


Tools for the Production of KML- or CSV Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* https://www.freemaptools.com/kml-file-creator.htm
* https://www.latlong.net/
* https://mapsengine.google.com/map/?hl=de
* https://geobrowser.de.dariah.eu/edit (Datasheet Editor :-)


Example Files
^^^^^^^^^^^^^

KML
    * https://steinheim-institut.de/daten/epidat.kml (`open in Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://steinheim-institut.de/daten/epidat.kml>`__)
    * https://geobrowser.de.dariah.eu/data/goethe.kml (`open in Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://geobrowser.de.dariah.eu/data/goethe.kml>`__)
    * https://geobrowser.de.dariah.eu/data/rechtegewalt.kml (`open in Geo-Browser <https://geobrowser.de.dariah.eu/?kml=https://geobrowser.de.dariah.eu/data/rechtegewalt.kml>`__)

CSV
    * :download:`Universal-Kirchenzeitung-Nachrichten-aus-der-Israelitschen-Abteilung.csv <examples/Universal-Kirchenzeitung-Nachrichten-aus-der-Israelitschen-Abteilung.csv>`


GIT Repository
--------------

DARIAH-DE Geo-Browser Repository
    * https://projects.gwdg.de/projects/geo-browser/repository

GeoTemCo
    * https://github.com/stjaenicke/GeoTemCo

PLATIN fork used in the DARIAH-DE Geo-Browser (mainly adapted configuration)
    * https://github.com/DARIAH-DE/PLATIN


Bug Tracking
------------

    * https://projects.gwdg.de/projects/geo-browser/work_packages
