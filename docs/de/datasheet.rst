.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
Der Datasheet Editor
====================

Der DARIAH-DE Datasheet Editor kann unter http://geobrowser.de.dariah.eu/edit oder mit Klick auf „Datasheet Editor“ im Geo-Browser aufgerufen werden. Alle Funktionen sind ohne eine Registrierung bei DARIAH zugänglich. Der Geo-Browser wird im Tutorial :doc:`DARIAH-DE Geo-Browser <geobrowser>` dokumentiert.

Weitere Infos zu Geo-Browser und Datasheet Editor gibt es im `DARIAH-DE Portal <https://de.dariah.eu/geobrowser>`__.


GUI-Dokumentation
-----------------

Create a new Datasheet
^^^^^^^^^^^^^^^^^^^^^^

Die erste Option nach Aufruf der Seite ist entweder ein neues Datasheet zu erstellen oder ein vorhandenes Datasheet zu importieren. Mit Klick auf „Create an empty worksheet“ kann ein neues Dokument erstellt werden. Mit Klick auf „Datei auswählen“ kann eine CSV Datei vom lokalen Speicher in ein Datasheet geladen werden.

.. figure:: ../images/image_6.png

 CSV-Dateien können mithilfe von Microsoft Excel oder Open Office Calc erstellt werden. Unter Mac OS X können CSV-Dateien auch mit Numbers erstellt werden. In dem Datasheet können direkt die Daten

  *  „Name“ (Name eines Ortes)
  *  „Address“ (Adresse eines Ortes/Ortsname)
  *  „Description“ (Beschreibung)
  *  „Longitude“ (Längengrad)
  *  „Latitude“ (Breitengrad)
  *  „TimeStamp“ (Zeitpunkt)
  *  „TimeSpan:begin“ (Beginn einer Zeitspanne)
  *  „TimeSpan:end“ (Ende einer Zeitspanne) und
  *  „GettyID“ (ID zur Identifikation von Orten, wird automatisch generiert bei der Getty-Abfrage)

eingetragen werden. Es ist nicht nötig, alle Daten einzutragen, um ein Datasheet zu erstellen.

.. figure:: ../images/image_7.png

Beispiel für eine Eingabe: Eine Stadt zu verschiedenen Zeitpunkten (TimeStamp) in der Geschichte. In der Beschreibung (Description) könnte die jeweilige Einwohnerzahl zum angegebenen Zeitpunkt stehen. In das Datasheet können Daten direkt über Copy und Paste von z. B. Excel oder Calc eingetragen werden. Dazu muss der Bereich der Tabelle markiert werden, der kopiert werden soll und dann in das Feld eingefügt werden, wo der markierte Bereich links oben anfangen soll. Es kann an beliebiger Stelle im Datasheet eingefügt werden.

Ebenfalls kann durch Rechtsklick auf ein Feld im Datasheet eine Zeile darüber („Insert row above“) oder darunter eingefügt werden („Insert row below“). Zeilen können durch „Remove row“ und Reihen durch „Remove column“ gelöscht werden. Reihen können ebenfalls links („Insert column on the left“) oder rechts („Insert column on the right“) eingefügt werden. Dadurch ist es möglich, neue Kategorien im Datasheet zu erstellen, falls zusätzliche Information zu den Orten gespeichert werden soll. Es können auch Kategorien im Editor verändert werden.

.. figure:: ../images/image_8.png


Place Selection
^^^^^^^^^^^^^^^

Beim direkten Eintragen eines Ortes in den Editor ist es nötig, den Längengrad und den Breitengrad zu kennen. Falls diese Daten nicht bekannt sind, gibt es dafür auf der Seite die Tools „Place Selection“ und „Add Georcoordinates“, um diese zu ermitteln. Unter der Weltkarte ist ein Feld, in den ein Ortsname eingetragen werden kann. Durch das Klicken auf einen der Suchbuttons TGN oder OSM wird (wenn der Ort gefunden werden kann) der Längen- und der Breitengrad ermittelt. TGN bekommt seine Daten vom Getty Thesaurus of Geographic Names. Bei dieser Suchoption werden größere Orte gefunden, kleinere Orte werden durch TGN nicht immer gefunden. OSM (OpenStreetMap) findet oft auch kleinere Orte. Durch OSM ist es auch möglich, Straßen und Hausnummern zu finden.

.. figure:: ../images/image_9.png

Grundsätzlich ist es möglich, eine weitere Option anzuklicken, falls ein Suchbutton keine Ergebnisse liefert. Eine weitere Möglichkeit ist, in der Map Selection mit der Maus die Fahne auf der Karte zu verschieben, dadurch lassen sich sehr genaue Längen- und Breitengrade bestimmen. Durch den Zoom in die Karte sieht man Straßen genauer.

.. figure:: ../images/image_10.png

Falls der ermittelte Ort nicht der gewünschte Ort ist, gibt es die Option, rechts neben dem eingetragenen Ortsnamen auf den Pfeil zu klicken, wodurch sich eine Liste an anderen (falls vorhanden) Orten mit dem gleichen Namen öffnet.

Über der Karte befindet sich ein Feld „Set all“. Wenn der Name dort und ebenfalls im Datasheet im Feld „Address“ eingegeben ist, werden durch das Klicken auf „Set All“ die Felder „Longitude“ und „Latitude“ entsprechend ausgefüllt.

.. figure:: ../images/image_11.png

Nachdem ein neuer Ort in der Karte gesucht wurde, öffnet sich oben ein grünes Fenster „New Datasheet created“. Durch „Bookmark this sheet“ hat das erstellte Datasheet eine eigene ID im DARIAH-Storage, worüber das Sheet wieder aufgerufen werden kann.


Add Geocoordinates
^^^^^^^^^^^^^^^^^^

Mit Klick auf „Geolocation Completion“ können der Längen- und Breitengrad und die GettyID eines Ortes automatisch ergänzt werden. Dazu muss nur das Feld „Address“ ausgefüllt werden.


Access Your Data
^^^^^^^^^^^^^^^^

Sobald ein neues Worksheet erstellt wird, wird der Datei eine ID im DARIAH-Storage Service zugeteilt. Über die ID lässt sich das Worksheet wieder aufrufen. Beim Klick auf „Download CSV“ lässt sich die Datei als CSV-Datei herunterladen und im lokalen Speicher speichern. Außerdem wird die Storage ID angezeigt und die gesamte URL kann durch Klick auf den Button „Copy URL to clipboard“ in den Zwischenspeicher kopiert werden, zwecks direkter Adressierung der Datei im DARIAH-Storage.


Open with Geo-Browser
^^^^^^^^^^^^^^^^^^^^^

Unter diesem Punkt können die in das Datasheet eingegebenen Daten mit dem Geo-Browser geöffnet werden. Dort werden die eingegebenen Orte als Punkte auf der Weltkarte angezeigt. Die Daten werden ebenfalls unter der Weltkarte in einer Liste angezeigt.


Actions
^^^^^^^

Oben rechts gibt es einen Button mit „Actions“. Durch das Klicken öffnet sich eine Liste mit Optionen. „New Datasheet“ öffnet ein neues Datasheet. Unter „Download CSV“ kann man eine Datei herunterladen, in der das aktuelle Datasheet als CSV Datei gespeichert ist. Diese kann z. B. mit Excel oder Calc geöffnet werden (Trennzeichen sind hier , und "). Beim Klick auf „Save“ werden die Daten im Datasheet gespeichert. Durch „Bookmark“ kann ein Lesezeichen für die Seite erstellt werden. Wenn bei „Autosave“ ein Häkchen gesetzt wird, werden neue Daten im Datasheet automatisch gespeichert.


Versionierung and Release
-------------------------

Die aktuelle Version des DARIAH-DE Datasheet Editors ist immer zu erreichen unter https://geobrowser.de.dariah.eu/edit. Seit April 2019 ist das die Release Version 2.7.0. Wir versuchen, neue Versionen abwärtskompatibel zu halten. Die aktuelle Entwicklungsversion des Datasheet Editors ist, soweit vorhanden, zu erreichen unter https://geobrowser.de.dariah.eu/beta/edit.


GIT Repository
--------------

    * https://projects.gwdg.de/projects/geo-browser/repository/revisions/master/edit


Bugtracking
-----------

    * https://projects.gwdg.de/projects/geo-browser/work_packages
