.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


===============
Geo-Browser FAQ
===============


.. topic:: Auf einer Karte würde ich gerne die verschiedenen Lebensstationen (Wohnorte) mehrerer Schriftsteller anzeigen. Besteht die Möglichkeit, sich zwei oder mehr komplette Lebenswege gleichzeitig anzeigen zu lassen, um die Biographien miteinander vergleichen zu können?

    Ja, wenn Sie pro Schriftsteller eine Datei/Tabelle erstellen, können Sie alle diese Dateien im Geo-Browser gleichzeitig visualisieren und vergleichen. Pro Datei (Schriftsteller) werden dann jeweils die Lebensstationen auf der Karte, in der Zeitlinie und in der Tabelle in verschiedenen Farben dargestellt. Hier ein `Beispiel <http://geobrowser.de.dariah.eu/index.html?kml1=http://geobrowser.de.dariah.eu/data/goethe.kml&kml2=http://geobrowser.de.dariah.eu/data/schiller.kml>`__ der Publikationsorte und -Zeiten von Goethe und Schiller aus dem OPAC der SUB Göttingen.

    Zum Hinzufügen eines neuen Datensatzes zu Ihrem aktuell dargestellten wählen Sie bitte über die load Data-Funktion eine beliebige lokale, CSV oder KML/KMZ-Datei aus und laden Sie diese per Klick auf den load-Button als weitere Quelle zum Geo-Browser hinzu.


.. topic:: Wir möchten gerne den Geo-Browser in unsere Website einbetten, wie ist das möglich?

    Der Geo-Browser kann gerne in unserer `Embedded-Version <http://geobrowser.de.dariah.eu/embed/>`__ in Webseiten eingebettet werden. Zur Darstellung müssten Sie nur eine URL mit Verweisen auf eine bzw. mehrere CSV-  oder KML-Dateien mitgeben, z. B.

    http://geobrowser.de.dariah.eu/embed/?kml=http://geobrowser.de.dariah.eu/data/goethe.kml

    oder

    http://geobrowser.de.dariah.eu/embed/index.html?kml1=http://geobrowser.de.dariah.eu/data/goethe.kml&kml2=http://geobrowser.de.dariah.eu/data/schiller.kml.

    Die URLs können als Magnetic Link abgerufen werden. Hierzu laden Sie die gewünschten Dateien in den Geo-Browser und kopieren dann den Link, der sich unter Dataset und Magnetic Link befindet (Rechtsklick mit der Maus und im Kontextmenü dann „Linkadresse kopieren“ auswählen, die URL befindet sich dann in der Zwischenablage).

    Genau wie die URL http://geobrowser.de.dariah.eu/ wird auch die URL der Embedded-Version stabil bleiben (siehe `Geo-Browser <https://wiki.de.dariah.eu/display/publicde/Geo-Browser+Dokumentation#Geo-BrowserDokumentation-VersionierungundRelease>`__ und `Datasheet Editor <https://wiki.de.dariah.eu/display/publicde/Datasheet+Editor+Dokumentation#DatasheetEditorDokumentation-VersionierungundRelease>`__ Versionierung und Release).


.. topic:: Welches Kartenmaterial wird im Geo-Browser genutzt? Können wir das Material nachnutzen?

    Als Grundkarte nutzen wir momentan `OpenStreetMap <https://www.openstreetmap.org/>`__. Das Kartenmaterial der historischen Karten wurde im Rahmen des Projekts `europeana4d <http://labs.europeana.eu/apps/Europeana4D/>`__ entwickelt, bzw. als Public Domain-Material genutzt. Nähere Infos könnten Sie evtl. noch auf den Seiten des `originären e4D-Dienstes <http://www.informatik.uni-leipzig.de:8080/e4D/>`__ finden. Diese Karten verwenden wir auch in unserem `GeoServer <https://ref.de.dariah.eu/geoserver/web/>`__, der den Geo-Browser bedient. Unter `Layer-Vorschau <https://ref.de.dariah.eu/geoserver/web/H3YQtIyw50o4teVI-BQjXBzecL2pbSEacJIklbLAtXrfIgS6fGUWeukMclexVZpZ4IPY3tcpwbDJYV8h55zAlg/H3Y2d/JYV66>`__ können Sie auch gerne die Karten herunterladen und nachnutzen.


.. topic:: Welchen Datentyp kann ich bei Load Overlay unter der Option ArcGIS WMS und XYZ Layer hochladen?

    Beides sind keine Dateiformate sondern Layer-Typen/Services für Overlays, die in der Karte angezeigt werden sollen. Bei ArcGIS WMS kann eine WMS-Server-URL und der Layer angegeben werden, der angezeigt werden soll. Bei XYZ Layer kann eine URL mit x/y/z Platzhaltern im `OpenLayers-Format <http://dev.openlayers.org/docs/files/OpenLayers/Format-js.html>`__ eingetragen werden.


.. topic:: Kann ich eigenes Kartenmaterial in den Geo-Browser einbinden?

    Wir bieten die historischen Karten, die im Geo-Browser angeboten werden, momentan über einen `GeoServer <https://ref.de.dariah.eu/geoserver/web//>`__ an, der verschiedenste Formate für Kartenmaterial unterstützt. Das Einbinden von digitalisierten Karten in den GeoServer ist durchaus möglich, jedoch noch nicht direkt für Sie als Nutzerin oder Nutzer. Eigene Karten müssten für den GeoServer vor- bzw. nachbereitet und dann (von uns) hochgeladen werden. Dies können wir gerne tun, sofern lizenzrechtliche Fragen geklärt sind und die Karten für andere Geo-Browser-Nutzerinnen und -Nutzer von Interesse sind. Nähere technische Details finden Sie in der `GeoServer-Dokumentation <http://docs.geoserver.org/>`__.

    Es gibt jedoch die Möglichkeit, über Load Overlay eigenes Kartenmaterial über die vorhandenen Karten zu blenden (siehe bspw. Roman Empire).


.. topic:: Warum kann ich unter Load Data keine KML, KMZ und CSV-Dateien über KML/KMZ/CSV File URL einbinden?

    Momentan sind die HTTP-Anfragen von JavaScript an externe URLs über einen Proxy-Server abgesichert. Somit können nur Dateien über eine URL von Servern in den Geo-Browser geladen werden, die in unserem Proxy-Server eingetragen sind. Gerne tragen wir Ihren Hostnamen in unseren Proxy-Server ein, so dass Ihre Dateien für Sie und andere Nutzerinnen und Nutzer im Geo-Browser verfügbar sind. Bitte haben Sie Verständnis dafür, dass wir nur offizielle bzw. institutionelle URLs eintragen. Melden Sie sich gerne per `E-Mail <info@de.dariah.eu>`__.


.. topic:: Ist es möglich, ein JPG im Datasheet zu integrieren, um ein Bild im Geo-Browser anzeigen zu lassen, oder auch anderen HTML-Code (auch mit CSS) wie z. B. URLs als Link zu präsentieren?

    Ja, dies ist direkt möglich im „Description“-Feld mittels HTML-Code. <img>-, <a>-Tags und CSS-Code kann dort direkt formatiert werden. Beispiel: http://geobrowser.de.dariah.eu/?kml1=http://geobrowser.de.dariah.eu/data/flickr/Earthquake.kml.
