.. Geo-Browser documentation master file, created by
   sphinx-quickstart on Wed May 20 16:07:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


====================
The Datasheet Editor
====================


The DARIAH-DE Datasheet Editor is accessible at http://geobrowser.de.dariah.eu/edit or by clicking “Datasheet Editor” on Geo-Browser. All functions can be used without a DARIAH-DE registration. Geo-Browser is documented in the tutorial :doc:`DARIAH-DE Geo-Browser <geobrowser>`.

Further information on the Geo-Browser and the Datasheet Editor are on the `DARIAH-DE Portal <https://de.dariah.eu/geobrowser>`__.


GUI Documentation
-----------------

Create a new Datasheet
^^^^^^^^^^^^^^^^^^^^^^

The first option after calling the website is to either create a new Datasheet or to import an existing Datasheet. By clicking “Create an empty worksheet” a new document is generated. By clicking “Choose file” a CSV file from the local storage can be loaded into the Datasheet.

.. figure:: images/image_6.png

CSV files can be created with Microsoft Excel or Open Office Calc. In Mac OS X CSV files can be created with Numbers. In the Datasheet following data can be entered:

*   „Name“ (Name of the location)
*   „Address“
*   „Description“
*   „Longitude“
*   „Latitude“
*   „TimeStamp“ (point in time)
*   „TimeSpan:begin“ (begin of the time span)
*   „TimeSpan:end“ (end of the time span) and
*   „GettyID“ (ID for the identification of locations, is generated automatically with the Getty query)

It is not required to enter all data to create a Datasheet.

.. figure:: images/image_7.png

Example of an entry: A city at different points in time (TimeStamp) in history. In the Description the population at the given point in time may be entered. The data in the Datasheet can be dragged directly via copy and paste from e.g. Excel or Calc. For this the area to be copied must be marked and inserted in the cell, where the marked area should start in the left upper corner. Data can be inserted at any position in the Datasheet.

By right-clicking on a cell in the Datasheet a row above (“Insert row above”) or below (“Insert row below”) can be inserted. Rows can be removed via “Remove row” and columns via “Remove Column”. Columns can be inserted as well either to the left (“Insert column on the left”) or right (“Insert column on the right”). Thus it is possible to create new categories in the Datasheet if additional information on the locations is to be saved. Categories can also be altered in the Editor.

.. figure:: images/image_8.png


Place Selection
^^^^^^^^^^^^^^^

To enter a location directly into the Editor, the Longitude and Latitude must be known. If these data are not already known the tools on the website “Place Selection” and “Add Geo coordinates” can be used to find them out. Below the world map is a field, in which a location name can be entered. By clicking one of the search options TGN or OSM, if the location can be found, the longitude and latitude are returned (caution: case sensitive). TGN references data from Getty Thesaurus of Geographic Names. With this search option larger locations are found, smaller locations are not always found with TGN. OSM (OpenStreetMap) often also finds smaller locations, which TGN does not find. With OSM it is also possible to find streets and street numbers.

.. figure:: images/image_9.png

Generally it is possible to choose another option, if a search button does not return results. Another option is to drag the flag in the Map Selection with the mouse, hereby the longitude and latitude can be determined with more precision. By zooming into the map it’s possible to see streets more precisely.

.. figure:: images/image_10.png

If the location is not the desired one, there is the option to click on the arrow next to the location name to open the drop-down menu with a list of other locations with the same name (if they exist).

Above the map is the field “Set all”. If the name entered there is the same as in the field “address” in the Datasheet, clicking “Set all” fills in the corresponding fields “Longitude” and “Latitude” in the Datasheet.

.. figure:: images/image_11.png

After a new location is searched on the map a green window opens above “New Datasheet created”. By clicking “Bookmark this sheet” the new Datasheet gets an own ID in the DARIAH storage, with which the sheet can be retrieved again.


Add Geocoordinates
^^^^^^^^^^^^^^^^^^

By clicking “Geolocation Completion” the longitude, latitude and GettyID of a location can be added automatically. Only the field “Address” has to be filled for this .


Access Your Data
^^^^^^^^^^^^^^^^

When a new Worksheet is created, an ID from the DARIAH-Storage Service is assigned to the file. The Worksheet can be retrieved again with the ID. By clicking “Download CSV” the file can be downloaded as CSV file and stored in the local storage. Furthermore the storage ID is shown, and the complete URL can be copied to the clipboard by clicking “Copy URL to clipboard“. Using this URL the file can be addressed directly.


Open with Geo-Browser
^^^^^^^^^^^^^^^^^^^^^

With this action the data from the Datasheet can be opened in Geo-Browser. The entered locations are shown as points on the world map. The data is also shown below the world map in a list.


Actions
^^^^^^^

In the upper right corner is the button for “Actions”. Clicking it will show you a drop down menu with a list of options. “New Datasheet” opens a new Datasheet.  With “Download CSV” you can download a file with the current Datasheet saved as CSV data. This file can be opened with e.g. Excel or Calc (seperators here are , and “). By clicking “Save” the data in the Datasheet are saved. With “Bookmark” a bookmark for the site can be generated. If “Autosave” is checked new data in the Datasheet is automatically saved.



Versions and Releases
---------------------

The current version of the DARIAH-DE Datasheet Editor is always accessible at https://geobrowser.de.dariah.eu/edit. As of April 2019 this is Release version 2.7.0. We try to keep new versions compatible with older ones. The version of the Datasheet Editor that is currently being developed is accessible via https://geobrowser.de.dariah.eu/beta/edit.



GIT Repository
--------------

    * https://projects.gwdg.de/projects/geo-browser/repository/revisions/master/edit


Bug Tracking
------------

    * https://projects.gwdg.de/projects/geo-browser/work_packages
