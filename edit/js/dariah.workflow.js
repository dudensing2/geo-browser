
// global variables
var dsid; // dariah storage id
var map; // osm
var proj4326; // osm projection
var projmerc; // mercator projection
var locationPickerPoint;
var locationPickerLayer;
var dragFeature;


/**
 * Stuff to setup when DOM is ready
 */
$(document).ready(function(){
  initTable(); // init the table widget
  initMap(); // init the OpenStreetmap widget
  init();
});

function logWidth() {
  console.log("offwi: " + $('#sheetArea')[0].offsetWidth);
  console.log("wi: " + $('#sheetArea').width());
  //console.log("wi css: " + $('#console').css(width));
  console.log("owwi: " + $('#sheetArea').outerWidth(true));

  console.log("dt offwi: " + $('#dataTable')[0].offsetWidth);
  console.log("dt wi: " + $('#dataTable').width());
  //console.log("wi css: " + $('#console').css(width));
  console.log("dt owi: " + $('#dataTable').outerWidth(true));
}

function init() {

	// drag and drop handler for CSV documents to workarea
	$('#workArea')
		.bind('dragover', handleDragOver, false)
		.bind('drop', handleDropFileSelect);

	// action for fileuploadbutton
	$('#files').bind('change', handleFileSelect);
    $('#saveMenuAction').click(save);
	$('#bookmarkMenuAction').click(setBookmark);
    $('.downloadLink').click(download);

	// jQuery creates it's own event object, and it doesn't have a
	// dataTransfer property yet. This adds dataTransfer to the event object.
	// Thanks to l4rk for figuring this out!
	jQuery.event.props.push('dataTransfer');

	// load data from storage if hashtag supplied
	dsid = $.query.get('id');
	if(dsid!=='') {
		console.log('dariah storage id given:' + dsid);
		loadFromDariahStorage(dsid);
		document.title = document.title + ' ' + dsid;
        url = 'https://geobrowser.de.dariah.eu/storage/' + dsid;
        $('#dataLoc input').val(dsid);
        $('#dataLoc button').attr('data-clipboard-text', url);
	} else {
        showFirstStartWizard();
  }

	$('input[name=autosave]').click(function () {
		if($(this).is(':checked')) {
      showMessage('Changes will be autosaved.');
		} else{
      showMessage('Changes will not be autosaved.');
		}
  });

	// http://duckranger.com/2012/06/pretty-file-input-field-in-bootstrap/
	// sadly doesn't work in chromium on linux
	/*$('input[id=files]').change(function() {
    $('#filename').val($(this).val());
	});*/

	//$('#centerOSMMarker').tooltip();
	//$('#centerOSMMarker').click(function())

}

function autoSave(change) {
  var message;
	if ($('input[name=autosave]').is(':checked')) {
		if(change) {
			message = 'Autosaved (' + change.length + ' cell' + (change.length > 1 ? 's' : '') + ')';
		} else {
			message = 'Autosaved changes.';
		}
		showMessage(message);
		autosaveNotification = setTimeout(function(){
      showMessage('Changes will be autosaved.');
		}, 1000);
		save();
	}
}


/**
 * Functions for loading file into Browser, drag'n'drop or with button
 * look at
 * - http://www.html5rocks.com/de/tutorials/file/dndfiles/
 * - http://weblog.bocoup.com/using-datatransfer-with-jquery-events/
 */
function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object
	loadFromFiles(files);
}

function handleDropFileSelect(evt) {
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files; // FileList object.
	loadFromFiles(files);
}

function handleDragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function loadFromFiles(files) {
	for (var i = 0, f; f = files[i]; i++) {
		console.log(f.name + ' - ' + f.type || 'n/a' + ' - ' + f.size + ' bytes, last modified: ' + f.lastModifiedDate.toLocaleDateString());

		if($.inArray(f.type, allowedMimeTypes) < 0 ) {
      newAlert('error', 'Could not open file', 'Mime-Type "' + f.type + '" not supported. If your file is a CSV file, maybe it is missing a .csv file suffix?', '');
			continue;
		}

		var reader = new FileReader();
		reader.onload = function(e) {
			var data = e.target.result;
			csvToTable(data);
			postToDariahStorage(data);
			newSheetAlert();
		};

		reader.readAsText(f);
	}
}

function csvToTable(data) {
	var arr = jQuery.csv()(data);

	// first uncheck autosave
	var asState = $('input[name=autosave]').is(':checked');
	$('input[name=autosave]').attr('checked', false);

	$('#dataTable').handsontable('loadData', arr);

	// colHeaders kind of broken...? this fixes, but whats the reason?
	$('#dataTable').handsontable("updateSettings", {colHeaders: true, rowHeaders:true });

    // reset autosave to beforestate
	$('input[name=autosave]').attr('checked', asState);

	// show alert
	hideFirstStartWizard();
}

var saveTimer;
function save() {
	var arr = $('#dataTable').handsontable('getDataReference');
	var csv =  arr2csv(arr);
	//var id=$.query.get('id');

	//console.log('save called');

	// make sure that not more than one save per second occur
	clearTimeout(saveTimer);
	saveTimer = setTimeout(function(){
		updateDariahStorage(dsid, csv);
		console.log('save executed');
	}, 1000);

	//switchSteps('#uploadData', '#dataToGeobrowser');
}

function download() {
  //console.log("trying download");
  var csv = encodeURI(arr2csv($('#dataTable').handsontable('getDataReference')));
  $(this).attr('href', 'data:text/csv;charset=utf-8,' + csv);
  $(this).attr('download', dsid + '.csv');
}

function hideFirstStartWizard() {
	$('#firstStartWizard').hide();
	$('#sheetArea').show();

	// check if data in sheet is fine
	//checkColumnHeaders();
}

function showFirstStartWizard() {
	$('#firstStartWizard').show();
	$('#sheetArea').hide();

	// check if data in sheet is fine
	//checkColumnHeaders();
}

function createNew() {
	postToDariahStorage();
	$('#dataTable').handsontable('loadData', [e4dHeader]);
	// colHeaders kind of broken...? this fixes, but whats the reason?
	//$('#dataTable').handsontable("updateSettings", {colHeaders: false, rowHeaders:false });
	//$('#dataTable').handsontable("updateSettings", {colHeaders: true, rowHeaders:true });
	//$('#dataTable').handsontable("updateSettings", {colHeaders: e4dHeader});
	newSheetAlert();
}

function newSheetAlert() {
	hideFirstStartWizard();
	var title = 'New Datasheet created';
	var message = '	<p>Your sheet has a unique ID in the DARIAH-DE Storage, where you can open'
              + ' it again.</p>'
              + '<p><button class="btn btn-success" onclick="setBookmark()">Bookmark this sheet</button>';
	newAlert('success', title,  message);
}

function openGeoBrowser(newWindow) {
  // check all at once
  var val1 = checkColumnHeaders();
  var val2 = checkDates();
  var val3 = checkCoordinates();
  if (! (val1 && val2 && val3) ) return;

  //if (!checkDates()) return;
  //if (!checkCoordinates()) return;
	var url = e4dURL+'?csv1='+(encodeURIComponent(storageURL+dsid));
	console.log(url);
	if(newWindow) {
		window.open(url);
	} else {
		document.location.href = url;
	}
}

function newAlert (type, title, message, extraClass) {
	if (extraClass === undefined) extraClass = '';

	elem = '<div class="alert alert-'+type+' alert-block fade in '+extraClass+' ">'
        + '<a class="close" data-dismiss="alert" href="#">×</a>';
	if(title!=="") elem += '<h4 class="">'+title+'</h4>';
	elem += message + '</div>';

  $("#alert-area").append($(elem));
    //$(".alert-message").delay(2000).fadeOut("slow", function () { $(this).remove(); });
}

function showMessage(text) {
	$('#console').text(text);
}
