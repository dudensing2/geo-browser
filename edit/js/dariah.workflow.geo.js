/*
 * functions for geographic checks & maps
 *
 */

// the api.geonames server seems to have a problem with umlauts, ws.geonames is fine
// fix OGN bei gelegenheit!
var ognURL = 'http://ws.geonames.org/postalCodeSearchJSON';
var ognUsername = 'dariahde';
var osmSearchUrl = 'https://nominatim.openstreetmap.org/search';

function initMap() {
	proj4326 = new OpenLayers.Projection("EPSG:4326");
	projmerc = new OpenLayers.Projection("EPSG:900913");

	map = new OpenLayers.Map("osmMap", {
		units: 'm',
		projection: projmerc,
		displayProjection: proj4326
	});
	var osmLayer = new OpenLayers.Layer.OSM("OpenStreetMap");
	map.addLayer(osmLayer);

	map.setCenter(0, 0);
}

/*
function startTGN() {
	$('#tgnArea').show();

	// do nothing if there are columns missing
	if(!checkColumnHeaders()) return;

	var arr = $('#dataTable').handsontable('getDataReference');

	$('#tgnList').empty();

	var addressColumn = findColumn(arr, 'Address');
	//console.log('addresscol is ' + addressColumn);

	// get list of places from multidimensional array,
	// do not return the "Address" header or emtpy strings
	var places = $.map(arr, function(x) {
		var ret = x[addressColumn];
		if(!isEmptyString(ret) && ret !== 'Address') {
			// remove whitespaces
			return $.trim(ret);
		}
	});

	// somehow sort() & $.unique() failed on chromium, this solution using underscore.js works:
	//http://stackoverflow.com/questions/4833651/javascript-array-sort-and-unique
	var sortUnique = _.compose(_.uniq, function(array) {
		return _.sortBy(array, _.identity);
	});

	places = sortUnique(places);

	// "mokeln" to get first index of dataset where lat and long is not empty.
	var isFirst = false;
	$.each(places, function(index, place) {
		$.get(tgnURL, {q : place, georef : 'true'}).success(function(xml){
			var elemId = 'tgn_'+place.replace(' ', '_');
			var elem = '<div class="input-append">'
				+ '<h4>'+place+':</h4>'
				+ '<p><select style="width:55%;" id="'+elemId+'"></select>'
				+ '<button class="btn" style="width:20%;" id="pick_'+elemId+'"><i class="icon-eye-open"></i> Map</button>'
				+ '<button class="btn" style="width:25%;" id="setAll_'+elemId+'"><i class="icon-pencil"></i> Set All</button>'
				+ '</p></div>';

			$('#tgnList').append(elem);
			// bind function to change event
			$('#'+elemId).change(placeChanged);
			$('#pick_'+elemId).bind('click', pickPlace);
			$('#setAll_'+elemId).bind('click', setAllPlacesTGN);
    			$(xml).find('term').each(function (index, val) {
				var id = $(this).attr('id');
				id=id.substring(id.indexOf(':')+1);
				var path = $(this).find('path').text();
				var name = $(this).find('name').text();
				// check for empty or not existing name tag, use match_name instead.
				if (name == "") {
					name = $(this).find('match_name').text();
				}
				var lat = $(this).find('latitude').text();
				var long = $(this).find('longitude').text();

				// only use dataset if lat and long are existing.
				if (lat != "" && long != "") {
					// "mokeln": see above.
					if (!isFirst) {
						var firstIndex = index;
						isFirst = true;
					}
					$('#'+elemId).append('<option value="'+name+'|'+id+'|'+lat+'|'+long+'">'+ name + ' : ' + path+'</option>');
				}

				// set coordinates of place to first returned entry
				if(index===firstIndex) {
					updateCoordinates(name, lat, long, id);
				}
			});
		});

	});
}
*/

/*
 *
 */
function startTGN() {
	$('#tgnArea').show();

	// do nothing if there are columns missing
	if(!checkColumnHeaders()) return;

	var arr = $('#dataTable').handsontable('getDataReference');

	$('#tgnList').empty();

	var addressColumn = findColumn(arr, 'Address');

	// get list of places from multidimensional array,
	// do not return the "Address" header or emtpy strings
	var places = $.map(arr, function(x) {
		var ret = x[addressColumn];
		if(!isEmptyString(ret) && ret !== 'Address') {
			// remove whitespaces
			return $.trim(ret);
		}
	});

	// somehow sort() & $.unique() failed on chromium, this solution using underscore.js works:
	//http://stackoverflow.com/questions/4833651/javascript-array-sort-and-unique
	var sortUnique = _.compose(_.uniq, function(array) {
		return _.sortBy(array, _.identity);
	});

	places = sortUnique(places);

	$.each(places, function(index, place) {
        var placeAlreadySetInTable = false;
		$.get(tgnURL, {q : place, georef : 'true'}).success(function(xml){
			var elemId = 'tgn_' + place.replace(' ', '_');
			var elem = '<div class="input-append">'
				+ '<h4 style="margin-bottom:3px;">' + place + '</h4>'
				+ '<p><select style="width:64%;" id="' + elemId + '"></select>'
				+ '<button class="btn" style="width:18%;" id="pick_' + elemId + '" title="Show this place in the map selection the right."><i class="icon-eye-open"></i>&nbsp;Map</button>'
				+ '<button class="btn" style="width:18%;" id="setAll_' + elemId + '" title="Sets all places with this name to the choosen coordinates."><i class="icon-pencil"></i>&nbsp;Set</button>'
				+ '</p></div>';

            console.log("elem " + elemId + ": ", elem);

			$('#tgnList').append(elem);
			// Bind function to change event.
			$('#' + elemId).change(placeChanged);
			$('#pick_' + elemId).bind('click', pickPlace);
			$('#setAll_' + elemId).bind('click', setAllPlacesTGN);
    		$(xml).find('term').each(function (index, val) {
				var id = $(this).attr('id');
				id = id.substring(id.indexOf(':') + 1);
				var path = $(this).find('path').text();
				var name = $(this).find('name').text();
                var variant = $(this).find('variant').text();

				// Check for empty or not existing name tag, use match_name instead.
				if (name == "") {
					name = $(this).find('match_name').text();
				}
				var lat = $(this).find('latitude').text();
				var long = $(this).find('longitude').text();

                // Only add places with exact match or variant involved!
                if (place === name || variant.includes(place)) {
                    $('#' + elemId).append('<option value="' + name + '|' + id + '|' + lat + '|' + long + '">' + name + ': ' + path + '</option>');
                }

                // Set coordinates of place to first returned entry with exact title match.
                if (!placeAlreadySetInTable && (place === name || variant.includes(place))) {
				    updateCoordinates(name, lat, long, id, false, variant);
                    placeAlreadySetInTable = true;
                }

			});
		});
	});
}

function setOSMapCenter(lat, long, zoomLevel) {
	$('#osmLat').val(lat);
	$('#osmLong').val(long);

	var mapCenterPositionAsLonLat = new OpenLayers.LonLat(long, lat); // Center of the map
	var mapCenterPositionAsMercator = mapCenterPositionAsLonLat.transform(proj4326, projmerc); // transform from WGS 1984 to Spherical Mercator Projection

	map.setCenter(mapCenterPositionAsMercator, zoomLevel);

	// place picker
	if(dragFeature !== undefined) dragFeature.destroy();
	if(locationPickerLayer !== undefined ) locationPickerLayer.destroy();

	locationPickerLayer = new OpenLayers.Layer.Vector("LocationPicker Marker");
	map.addLayer(locationPickerLayer);
	locationPickerPoint = new OpenLayers.Geometry.Point(mapCenterPositionAsMercator.lon, mapCenterPositionAsMercator.lat);
	var locationPickerMarkerStyle = {externalGraphic: 'img/flag.png', graphicHeight: 37, graphicWidth: 32, graphicYOffset: -37, graphicXOffset: -16 };
	var locationPickerVector = new OpenLayers.Feature.Vector(locationPickerPoint, null, locationPickerMarkerStyle);
	locationPickerLayer.addFeatures(locationPickerVector);

	dragFeature = new OpenLayers.Control.DragFeature(locationPickerLayer,
	{
		onComplete:  function( feature, pixel ) {
			var selectedPositionAsMercator = new OpenLayers.LonLat(locationPickerPoint.x, locationPickerPoint.y);
			var selectedPositionAsLonLat = selectedPositionAsMercator.transform(projmerc, proj4326);
			$('#osmLat').val(selectedPositionAsLonLat.lat);
			$('#osmLong').val(selectedPositionAsLonLat.lon);
		}
	});
	map.addControl(dragFeature);
	dragFeature.activate();
}

function pickPlace() {
	var lat, long, zoomLevel;
	var name = $(this).attr('id').split('_')[2];
	var selectedId = '#tgn_' + name;

	if($(selectedId).val() === null) {
		lat = 0;
		long = 0;
		zoomLevel = 0;
	} else {
		var vals = $(selectedId).val().split('|');
		lat = vals[2];
		long = vals[3];
		zoomLevel = 12;
	}

	setOSMapCenter(lat, long, zoomLevel);
	$('#osmPlaceName').val(name);
	$('#osmTgnSearch').val(name);
	$('#tgnOsmList').empty();
}

function placeChanged() {
	var vals = $(this).val().split('|');
	var address = vals[0];
	var id = vals[1];
	var lat = vals[2];
	var long = vals[3];

	/*$.get(tgnURL, {id: id}).success(function(xml) {
		console.log(xml);
	});*/
	setOSMapCenter(lat, long, 12);
	$('#osmPlaceName').val(address);
	$('#osmTgnSearch').val(address);
	$('#tgnOsmList').empty();
}

function setAllPlacesTGN() {
	// called from "Set All" button
	if($(this).attr("id").indexOf("setAll" === 0)) {
		var selectedId = '#tgn_' + $(this).attr('id').split('_')[2];
		var vals = $(selectedId).val().split('|');
	} else {
	// TODO: is this used?
	    var vals = $(this).val().split('|');
	}

	var name = vals[0];
	var gettyId = vals[1];
	var lat = vals[2];
	var long = vals[3];

	updateCoordinates(name, lat, long, gettyId, true);
}

function setAllPlacesOSM() {
	var name = $('#osmPlaceName').val();
	var gettyId = "";
	var lat = $('#osmLat').val();
	var long = $('#osmLong').val();

	updateCoordinates(name, lat, long, gettyId, true);
}

function tgnSearch() {
	var place = $('#osmTgnSearch').val();

	console.log('TGN URL: ' + tgnURL);

	$.get(tgnURL, {q : place, georef : 'true'}).success(function(xml) {
		var datasets = $(xml).find('term');
		if($(datasets).length < 1) {
			$('#tgnOsmList').html('<p>No results for "' + place + '"</p>');
			return;
		}

		var elemId = 'tgn_osm_'+place.replace(' ', '_');

		var elem = '<div class="input-prepend">'
			+ '<span class="add-on">Show</span>'
			+ '<select id="'+elemId+'"></select>'
			+ '</div>';

		$('#tgnOsmList').html(elem);

		// bind function to change event
		$('#'+elemId).change(showOSMapPlace);

		// "mokeln" to get first index of dataset where lat and long is not empty.
		var isFirst = false;
		$(datasets).each(function (index, val) {
			var id = $(this).attr('id');
			id=id.substring(id.indexOf(':')+1);
			var path = $(this).find('path').text();
			var name = $(this).find('name').text();
			// check for empty or not existing name tag, use match_name instead.
			if (name == "") {
				name = $(this).find('match_name').text();
			}
			var lat = $(this).find('latitude').text();
			var long = $(this).find('longitude').text();

			// only use dataset if lat and long are existing.
			if (lat != "" && long != "") {
				// "mokeln": see above.
				if (!isFirst) {
					var firstIndex = index;
					isFirst = true;
				}
				$('#'+elemId).append('<option value="'+name+'|'+id+'|'+lat+'|'+long+'">'+ name + ' : ' + path+'</option>');
			}

			// show coordinates of first returned entry
			if(index===firstIndex) {
				setOSMapCenter(lat, long, 12);
			}
		});
	});
}

function ognSearch() {
	var place = $('#osmTgnSearch').val();

	$.getJSON(ognURL, {placename : place, username : ognUsername }).success(
		function(data) {
			if(data.postalCodes.length < 1) {
				$('#tgnOsmList').html('<p>No results for "' + place + '"</p>');
				return;
			}

			var elemId = 'tgn_osm_'+place.replace(' ', '_');
			var elem = '<div class="input-prepend">'
				+ '<span class="add-on">Show</span>'
				+ '<select id="'+elemId+'"></select>'
				+ '</div>';

			$('#tgnOsmList').html(elem);
			$('#'+elemId).change(showOSMapPlace);

			$.each(data.postalCodes, function(index, val) {
				var name = val.placeName;
				var id = '';
				var path = val.adminName3 + ' | ' + val.adminName1 + ' | ' + val.countryCode;

				if(index===0) {
					setOSMapCenter(val.lat, val.lng, 12);
				}

				$('#'+elemId).append('<option value="'+name+'|'+id+'|'+val.lat+'|'+val.lng+'">'+ name + ' : ' + path+'</option>');
		});
	});
}

function osmSearch() {
	var place = $('#osmTgnSearch').val();
	$.getJSON(osmSearchUrl, {q : place, format : 'json'}).success(function(data) {
		console.log(data);

		if(data.length < 1) {
			$('#tgnOsmList').html('<p>No results for "' + place + '"</p>');
			return;
		}

		var elemId = 'tgn_osm_'+place.replace(' ', '_').replace(',','_');
		var elem = '<div class="input-prepend">'
			+ '<span class="add-on">Show</span>'
			+ '<select id="'+elemId+'"></select>'
			+ '</div>';

		$('#tgnOsmList').html(elem);
		$('#'+elemId).change(showOSMapPlace);

		$.each(data, function(index, val){
			if(index===0) {
				setOSMapCenter(val.lat, val.lon, 12);
			}
			var id='';
			$('#'+elemId).append('<option value="'+val.display_name+'|'+id+'|'+val.lat+'|'+val.lon+'">'+ val.display_name+'</option>');
		});
	});
}

function showOSMapPlace() {
	var vals = $(this).val().split('|');
	var name = vals[0];
	var lat = vals[2];
	var long = vals[3];

	setOSMapCenter(lat, long, 12);
}

// TODO: merge functionality with checkDate, great would also be
// telling the linenumber
function checkCoordinates() {
	$(".coordAlert").alert('close');
	var arr = $('#dataTable').handsontable('getDataReference');
	var errorInRow = [];

	var latCol = findColumn(arr, 'Latitude');
	var longCol = findColumn(arr, 'Longitude');

	var result = true;

	$(arr).each(function(rowIndex, rowData){
		// don't check the header
		if(rowIndex !== 0) {
			var rvla = isValidLat(rowIndex, rowData, latCol);
			result = rvla && result;
			if (!rvla) errorInRow.push(errLink(rowIndex, latCol));
			var rvlo = isValidLong(rowIndex, rowData, longCol);
			result = rvlo && result;
			if (!rvlo) errorInRow.push(errLink(rowIndex, longCol));
		}
	});

//console.log(errorInRow);

	if(!result) {
		newAlert('error', 'Invalid coordinates', '<p>Some Geocoordinates are out of range.'
			+ 'Longitude may be between -180 and +180, Latitude beetween -90 and +90.'
			+ '</p><p>Row: ' + errorInRow.join(",")
			, 'coordAlert');
	}

	return result;
}

function errLink(row, col) {
	//var link = $('<a>', {text: row + 1});
	var link = '<a href="#" onclick="jumpToCell('+row+','+col+'); return false;">'+row+'</a>';

	return link;
}

function jumpToCell(row, col) {
	//this.preventDefault();
	setTimeout(function () {
		//timeout is needed because Handsontable normally deselects
		//current cell when you click outside the table
		$('#dataTable').handsontable("selectCell", row, col);
	}, 50);
}

/**
 * check if rowData contains a valid Latitude at rowIndex and ColumNumber,
 * mark errors in table
 *
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidLat(rowIndex, rowData, columnNumber) {
	var entry = rowData[columnNumber];
	var isValid = true;
	if(!isEmptyString(entry)) {
		isValid = ( entry >= -90 && entry <= 90);
		if(!isValid) {
			$('#dataTable').handsontable('getCell', rowIndex, columnNumber).className = 'error';
		}
	}

	return isValid;
}

/**
 * check if rowData contains a valid Latitude at rowIndex and ColumNumber,
 * mark errors in table
 *
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidLong(rowIndex, rowData, columnNumber) {
	var entry = rowData[columnNumber];
	var isValid = true;
	if(!isEmptyString(entry)) {
		isValid = ( entry >= -180 && entry <= 180);
		//console.log('valid? :' +  isValid);
		if(!isValid) {
    			$('#dataTable').handsontable('getCell', rowIndex, columnNumber).className = 'error';
		}
	}

	return isValid;
}
