/* date checking functions */

// regular expressions for datecheck, valid are xsd:date, xsd:dateTime, xsd:gYear and xsdGYearMonth
var xsdGYear = /^(-?)(\d*)$/;
var xsdGYearMonth = /^(-?)(\d*)-(0\d|1(0|1|2))$/;
// date or datetime (http://www.experts-exchange.com/Programming/Languages/Scripting/JavaScript/Q_21889287.html)
// Addad month schema from xsdYearMonth, see above. Day schema was not changed... -fu
var xsdDateOrDateTime = /(-)?(\d*)-(0\d|1(0|1|2))-(\d{2})(T(\d{2}):(\d{2}):(\d{2})(\.\d+)?)?(([\+-])((\d{2}):(\d{2})))?/;

var gYearSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77127.html';
var gYearMonthSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77135.html';
var dateSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77041.html';
var dateTimeSchemaUrl = 'http://books.xmlschemata.org/relaxng/ch19-77049.html';

function checkDates() {
	$(".dateAlert").alert('close');

	var arr = $('#dataTable').handsontable('getDataReference');
  var errorInRow = [];
	
	var timeStampCol = findColumn(arr, 'TimeStamp');
	var timeSpanBCol = findColumn(arr, 'TimeSpan:begin');
	var timeSpanECol = findColumn(arr, 'TimeSpan:end');
	
	var result = true;
	
	$(arr).each(function(rowIndex, rowData){
		// don't check the header
		if(rowIndex !== 0) {
			var rvd1 = isValidDate(rowIndex, rowData, timeStampCol);
      result = rvd1 && result;
      if (!rvd1) errorInRow.push(errLink(rowIndex, timeStampCol));
			var rvd2 = isValidDate(rowIndex, rowData, timeSpanBCol);
      result = rvd2 && result;
      if (!rvd2) errorInRow.push(errLink(rowIndex, timeSpanCol));
			var rvd3 = isValidDate(rowIndex, rowData, timeSpanECol);
      result = rvd3 && result;
      if (!rvd3) errorInRow.push(errLink(rowIndex, timeSpanECol));
      //if (!(rvd1 && rvd2 && rvd3)) errorInRow.push(rowIndex + 1);
		}
	});
	
	if(!result) {
		newAlert('error', 'Invalid date format', '<p>Some date fields have an unrecognized date format.'
              + 'Date needs to be <a target="_blank" href="'+gYearSchemaUrl+'">xsd:gYear</a>, '
              + '<a target="_blank" href="'+gYearMonthSchemaUrl+'">xsd:gYearMonth</a>,'
              + '<a target="_blank" href="'+dateSchemaUrl+'">xsd:date</a> '
              + 'or <a target="_blank" href="'+dateTimeSchemaUrl+'">xsd:dateTime</a>.'
              + '</p><p>Row: ' + errorInRow.join(",")
              , 'dateAlert'
		);
	}
	return result;
}

/**
 * check if rowData contains a valid datetime at rowIndex and ColumNumber,
 * mark errors in table
 * 
 * @param rowIndex
 * @param rowData
 * @param columnNumber
 * @return boolean if row was valid
 */
function isValidDate(rowIndex, rowData, columnNumber) {
		var entry = rowData[columnNumber];
		var isValid = true;
		if(!isEmptyString(entry)) {
			isValid = (xsdGYear.test(entry) || xsdGYearMonth.test(entry) || xsdDateOrDateTime.test(entry)); 
			if(!isValid) {
        $('#dataTable').handsontable('getCell', rowIndex, columnNumber).className = 'error';
			}
		}
		return isValid;
}
