/**
 * Functions dealing with DARIAH Storage up & download
 * TODO: jquery object
 */

 // create new file, get unique file id
function postToDariahStorage(postdata) {
	// re-set dsid
	dsid="";
	$.ajax({
		url: storageURL,
		type: 'POST',
		contentType: 'text/csv',
		data: postdata,
		success: function(data, status, xhr) {
			//console.log('loc: ' + xhr.getResponseHeader('Location'));
			var location = xhr.getResponseHeader('Location');
			dsid = location.substring(location.lastIndexOf('/') + 1);
			document.location.hash='id='+dsid;
            url = 'http://geobrowser.de.dariah.eu/storage/' + dsid;
            $('#dataLoc input').val(dsid);
            $('#dataLoc button').attr('data-clipboard-text', url);
		},
		error: function (data, text, error) {
			showMessage('Could not create new file in DARIAH storage: ' + text);
			console.log(data);
			console.log(text);
			console.log(error);
		}
		/*complete: function(xhr, status) {
			console.log(status);
			console.log('loc: ' + xhr.getResponseHeader('Location'));
			console.log('hdrs: ' + xhr.getAllResponseHeaders());
		}*/
	});
}


function loadFromDariahStorage(id) {
	$.get(storageURL+id)
		.success(function(data) {
			csvToTable(data);
		})
		.error(function() {
			showMessage('Error loading data with ID ' + id);
		});
}

// update file
function updateDariahStorage(id, postdata) {
	$.ajax({
		url: storageURL+id,
		type: 'PUT',
		contentType: 'text/csv',
		data: postdata,
		complete: function( response ) {
			console.log(response);
		}
	});
}
