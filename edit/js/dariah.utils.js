/*
 * utility functions
 */
 
function dump() {
	var arr = $('#dataTable').handsontable('getDataReference');
	console.log(arr2csv(arr));
}

// http://stackoverflow.com/questions/5828965/bookmark-on-click-using-jquery
function setBookmark() {
	console.log('trying to set bookmark');
	//e.preventDefault(); // this will prevent the anchor tag from going the user off to the link
	var bookmarkUrl = window.location;
	console.log('b:' + bookmarkUrl);
	var bookmarkTitle = document.title;
	
	if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) { 
			alert("This function is not available in Google Chrome. Click the star symbol at the end of the address bar or hit CTRL-D (Command-D for Macs) to create a bookmark.");
	}else if (window.sidebar) { // For Mozilla Firefox Bookmark
		window.sidebar.addPanel(bookmarkTitle, bookmarkUrl,"");
	} else if( window.external || document.all) { // For IE Favorite
		window.external.AddFavorite( bookmarkUrl, bookmarkTitle);          
	} else if(window.opera) { // For Opera Browsers
		$("a.bookmark").attr("href",bookmarkUrl);
		$("a.bookmark").attr("title",bookmarkTitle);
		$("a.bookmark").attr("rel","sidebar");
	} else { // for other browsers which does not support
    alert('Your browser does not support bookmarking from javascript, use your browsers bookmarking function.');
    return false;
	}
}

// TODO: also look at http://www.uselesscode.org/javascript/csv/
// and at handsontable csv2array function
// http://www.filosophy.org/bitstream/2011/12/17/js_array_to_csv.html
function arr2csv(arr){
	var csv = '';
	for(var row in arr){
		for(var col in arr[row]){
			var value = arr[row][col].replace(/"/g, '""');
			csv += '"' + value + '"';
			if(col != arr[row].length - 1){
				csv += ",";
			}
		}
		csv += "\n";
	}
	return csv;
}

function isEmptyString(str) {
    return (!str || ! str.length);
}
