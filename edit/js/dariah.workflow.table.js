/* function dealing with the handsontable widget
 *
 * TODO:
 * - encapsulate handsontable requests
 */


function initTable() {

	$("#dataTable").handsontable({
		rows: 40,
		cols: 10,
		rowHeaders: true, // 123
		colHeaders: true, // ABC
		minSpareCols: 1, //always keep at least 1 spare row at the right
		minSpareRows: 1, //always keep at least 1 spare row at the bottom
		contextMenu: true, // menu for add rows or cols,
		legend: [
      {
        match: function (row, col, data) {
          return (row === 0); //if it is first row
        },
        style: {
          color: 'green', //make the text green and bold
          fontWeight: 'bold'
        },
        title: 'Heading' //make some tooltip
      }
		],
		autoComplete: [
			{
				match: function (row, col, data) {
					//console.log('match called ' + row + '|' + col + '|' + data()[row][col]);
					return (row === 0 ); //if it is first column
				},
				source: function () {
					//console.log("sclaaed");
					return e4dHeader;
				}
			}
		],
		onChange: function (change, source) {
      console.log('onCHange called with source ' + source);

      //console.log('source' + source);
      if (source === 'loadData') {
        return; //don't save this change
      }
      autoSave(change);
		}

	});
}


/**
 * this function checks if every needed column is there
 */
function checkColumnHeaders() {

	// close all previous set col alerts
	//$('.columnAlert').each( function () { this.close(); });
	$(".columnAlert").alert('close');

  var arr = $('#dataTable').handsontable('getDataReference');
	var ready = true;

	$('#missingCols').empty();

	$(requiredColumns).each(function(index,val) {
		ready = findOrAlertColumn(arr, val) >=0 && ready;
	});

	$(addableColumns).each(function(index, val) {
		//findOrCreateColumn(arr, val);
		ready = findOrAlertCreatableColumn(arr, val) >=0 && ready;
	});

	return ready;

}

function findColumn(arr, columnName) {
	return $.inArray(columnName, arr[0]);
}

function findOrAlertColumn(arr, columnName) {
	var id = $.inArray(columnName, arr[0]);
	if(id<0) {
		//$('#missingCols').append('<li>'+columnName+'</li>');
		newAlert('error', 'Missing Column', '<p>Column <strong>' + columnName + '</strong> is missing. You should set a matching cell header with this name. Then try again.</p>', 'columnAlert');
	}
	return id;
}

function findOrAlertCreatableColumn(arr, columnName) {
	var id = $.inArray(columnName, arr[0]);
	if(id<0) {
		var message = '<p>You could create missing column <strong>'+ columnName +'</strong> and try again.'
                + '&nbsp;<button class="btn btn-warning" onclick="createColumn(\''+columnName+'\');">Create</button></p>';
		newAlert('error', 'Missing Column', message, 'columnAlert');
	}
	return id;
}

function createColumn(columnName) {
	var arr = $('#dataTable').handsontable('getDataReference');
	var id = $.inArray(columnName, arr[0]);

	// entry not existing, create
	if(id<0) {
		var colId;
		// find empty column header
		$(arr[0]).each(function(index,val) {
			if(val==='') {
				colId=index;
			}
		});

		//console.log('empty col: ' +  colId);
		$('#dataTable').handsontable('selectCell', 0, colId-1);
		$('#dataTable').handsontable('alter', 'insert_col', colId-1);
		$('#dataTable').handsontable('setDataAtCell', 0, colId-1, columnName);
		//console.log('tried:' + colId + ' ' + columnName);
	}

	return id;
}

/**
 * Table  is filled with coords and Getty IDs only if address is contained in table's address field
 * or address field is empty AND (if given) address field content is contained in variant string.
 *
 * Getty ID is only overwritten if also not existing!
 */
function updateCoordinates(address, lat, long, gettyId, forceOverwrite, variant) {

    // In case variant is not existing or given.
    if (!variant) {
        variant = "";
    }

    var arr = $('#dataTable').handsontable('getDataReference');

	var addressColumn = findColumn(arr, 'Address');
	var longColumn = findColumn(arr, 'Longitude');
	var latColumn = findColumn(arr, 'Latitude');
	var gettyColumn = findColumn(arr, 'GettyID');

	var save = false;

	$(arr).each(function(index, val) {
		if ($.trim(val[addressColumn]) === $.trim(address) || ($.trim(val[addressColumn]) !== "" && variant.includes($.trim(val[addressColumn])))) {
			if (forceOverwrite || (isEmptyString(val[longColumn]) && isEmptyString(val[latColumn]) && isEmptyString(val[gettyColumn]))) {
				$('#dataTable').handsontable('setDataAtCell', index, longColumn, long);
				$('#dataTable').handsontable('setDataAtCell', index, latColumn, lat);
				$('#dataTable').handsontable('setDataAtCell', index, gettyColumn, gettyId);
				save = true;
			}
		}
	});

	if(save) {
        autoSave();
    }
}
