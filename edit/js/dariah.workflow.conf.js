/*
 * config for workflow
 */

var host = window.location.protocol + '//' + window.location.hostname;

// DARIAH storage enpoint
var storageURL = 'https://geobrowser.de.dariah.eu/storage/';
// DARIAH TGNQuery enpoint
var tgnURL = 'https://ref.de.dariah.eu/tgn/v1/exact-match';
// URL for e4d browser
var e4dURL = host + '/';

// all possible headers (for new sheet)
var e4dHeader = ["Name", "Address", "Description", "Longitude", "Latitude", "TimeStamp", "TimeSpan:begin", "TimeSpan:end", "GettyID"];
// headers required to be set
var requiredColumns = ["Address", "TimeStamp", "TimeSpan:begin", "TimeSpan:end"]; // columns which need to be there
// colums which could be added automatically
var addableColumns = ["GettyID", "Longitude", "Latitude"]; // columns which may be added automatically
// mime types which pass check to get parsed into table
var allowedMimeTypes = ["text/csv", "application/vnd.ms-excel", "text/comma-separated-values"]
