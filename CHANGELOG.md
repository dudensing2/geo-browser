Release Version 2.7.8 (Juni 2020)
=================================

* Popup labels on mouseOver the Place circles now first take the “name“ value, and if empty the “place“ value. If both are not set, “unknown“ is used.
* Documentation slightly updated.


Release Version 2.7.0 (April 2019)
==================================

* Update of the `GeoServer <https://ref.de.dariah.eu/geoserver/>`__ to version 2.13.1.
* Documentation of the CSV file specification
* English translation of the FAQ section
* Content update of documentation
* Usage of the `Sphinx framework <http://sphinx-doc.org/>`__ for generation of documentation


Release Version 2.0.0 (Juli 2016)
=================================

* The `GeoServer <https://ref.de.dariah.eu/geoserver/>`__ that is providing the Geo-Browser with maps, has been updated to version 2.8.3.
* The continental borders of the historical maps have been corrected (GeoServer).
* Geo-Browser now does provide current EuroStat maps of 2013 and 2014 (GeoServer).
* The documentation has been translated to English.
* Licensing information is now provided directly in the map view (PLATIN).
* There is a possibillity to visualise local XLS/XLSX files using "Load Data" (PLATIN).
* Under "Load Overlay" the Water Layer of Maps-For-Free can be used (PLATIN).
* The global DARIAH-DE tool menu has been integrated.
* The PIWIK of GWDG was integrated, to track website-usage and browser information (taking data privacy into account). You can deactivate the tracking at the `Privacy Policy Page <https://de.dariah.eu/en/datenschutz>`__ page, or just cofigure your browser accordingly.
* The usage of `OpenGeoNames <http://www.geonames.org/>`__ in the Datasheet Editor is again possible.
* The code of Geo-Browser and Datasheet Editor has been merged into a `shared code repository <https://projects.gwdg.de/projects/geo-browser/repository>`__.
* `PLATIN <https://github.com/DARIAH-DE/PLATIN>`__ has been forked on Github for the usage of Geo-Browser in DARIAH-DE.
* The static data has been added to the PLAITN fork as well.


Release Version 1.0.0 (October 2015)
====================================

* Adapted to the DARIAH-DE Styleguide.
* Added information to documentation and FAQ.
* Update of the timeline module, so that time ranges can now be visualised, too.
