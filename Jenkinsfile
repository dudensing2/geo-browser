import groovy.json.JsonSlurper

node {

  def package_version
  def name
  def description
  def url
  def prefix
  def snapshot
  def beta

  stage('Preparation') {
    mvnHome = tool 'Maven 3.5.0'
    checkout scm
    sh 'rm -f *.deb'
    sh 'rm -rf PLATIN'

    // TODO: Set version number in the HTML pages, too! Decide between releases and snapshots: SNAPSHOTs are put into the SNAPSHOTS repo, releases and BETAs are put into the RELEASES repo!
    package_version = '2.7.8'
    name = 'dariahde-geobrowser'
    description = 'DARIAH-DE Geo-Browser'
    url = 'https://geobrowser.de.dariah.eu/'
    prefix = '/var/www/geobrowser.de.dariah.eu'
    snapshot = package_version.contains("SNAPSHOT")
    beta = package_version.contains("BETA")
  }

  stage('Build') {

    sh '''
      echo "===>  Building JavaScript and PLATIN things"
      git clone https://github.com/DARIAH-DE/PLATIN.git
      cd PLATIN && rake all
      cd ../edit && bower install

      echo "===>  Preparing doc building"
      umask 0022
      cd ../docs

      echo "===>  Setting up virtualenv"
      virtualenv venv
      pwd
      . venv/bin/activate

      echo "===>  Installing requirements"
      pip install -r requirements.txt

      echo "===>  Building HTML version within $(pwd)"
      make clean html
    '''
  }

  stage('Package') {
    // If BETA, configure fpm to build BETA DEB package for RELEASES repo
    if (beta) {
      prefix = '/var/www/geobrowser.de.dariah.eu/beta'
      name = 'dariahde-geobrowser-beta'
      description = 'DARIAH-DE Geo-Browser BETA'
      url = 'https://geobrowser.de.dariah.eu/beta/'
    }
    sh """fpm \
        -t deb -a noarch -s dir \
        --name ${name} \
        --description='${description}' \
        --maintainer='DARIAH-DE <info@de.dariah.eu>' \
        --vendor='DARIAH-DE' \
        --url=${url} \
        --iteration ${BUILD_NUMBER} \
        --version ${package_version} \
        --prefix ${prefix} \
        -C .
        """
  }

  stage('Publish') {
    if (snapshot) {
      doDebSnapshot(name, '.', package_version)
    }
    else {
      doDebRelease(name, '.', package_version)
    }
  }

}
